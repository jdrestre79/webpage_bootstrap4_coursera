# Diseñando páginas web con Bootstrap 4 - Coursera

![](https://bitbucket.org/jdrestre79/webpage_bootstrap4_coursera/raw/a25c99d060c3fa5234aca79305aa76539f39e226/images/Bootstrap4Readme.png)

## Acerca de este Curso
Este curso te enseñará conceptos generales de desarrollo web del lado cliente, metodologías de trabajo y herramientas. Aprenderás sobre diseño responsive, grillas, y componentes CSS y Javascript de Bootstrap. Practicarás con preprocesadores de CSS, Less y Sass. También aplicarás conceptos básicos de Node.js y NPM para gestionar tus sitios web.

Al finalizar este curso, habrás practicado con todas esas herramientas y serás capaz de diseñar una página web e implementar ese diseño con Bootstrap, crear un diseño responsive y preparar tu sitio web para salir a producción.

## Semana 1

Este módulo te dará una introducción a la programación web full stack y una rápida visión del curso completo en general. Aprenderás conceptos básicos de Bootstrap, inicializar un proyecto utilizando NPM, configurar tu repositorio utilizando Git y Bitbucket. Aprenderás sobre diseño responsive y sistemas de grillas de Bootstrap.

### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | Bienvenida a Bootstrap 4.0 y el desarrollo de UI
2 | ¿Qué es un programador full stack?
3 | Comenzando un proyecto
4 | Introducción a Bootstrap
5 | Diseño Responsive y sistema de grillas de Bootstrap


## Semana 2

Este módulo abarca los componentes CSS de Bootstrap. Aprenderás sobre Navegación y la barra de navegación, botones, formularios, tablas, tarjetas, imágenes y media, tags, alertas y barras de progreso.

### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | Navegabilidad y barra de navegación
2 | Formularios y botones
3 | Mostrando contenido con tablas y tarjetas
4 | Imágenes
5 | Notificaciones

## Semana 3

Este módulo comprende los componentes Javascript de Bootstrap. Aprenderás sobre pestañas, navegación con pestañas, navegación con botones, diferentes tipos de alertas, carroussel, scrollspy, affix y tooltips.

### Lista de lecciones
---
| Lección | Descripción |
|:------: | :------ |
| 1 | Componentes JS de Bootstrap |
| 2 | Pestañas y navegación por pestañas |
| 3 | Visibilidad |
| 4 | Desplegando contenido |
| 5 | Carroussel |

## Semana 4

Este módulo comprende los componentes Javascript de Bootstrap. Aprenderás sobre pestañas, navegación con pestañas, navegación con botones, diferentes tipos de alertas, carroussel, scrollspy, affix y tooltips.

### Lista de lecciones
---
Lección | Descripción
:---: | :---
1 | Bootstrap y JQuery
2 | Less y Sass
3 | Scripts NPM
4 | Task Runners Grunt y Gulp

## Autor
Juan David Restrepo Z - [Github](https://github.com/jdrestre) | [Twitter](https://twitter.com/jdrestre) | [Linkedin](https://www.linkedin.com/in/juandavidrestrepozambrano/) | [Bitbucket](https://bitbucket.org/jdrestre79/)